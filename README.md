# Comprehensive Pi-Hole Adlists

- - - -

A replacement adlist list for Pi-Hole, this list includes the default
Pi-Hole lists along with lists compiled from reddit.com/r/pihole.

This list is maintained at https://bitbucket.org/cyvaquero/pihole-adlists

Know of any other lists? Feel free to let us know about them by opening a
ticket in the project issue tracker -
    https://bitbucket.org/cyvaquero/pihole-adlists/issues

- - - -

## Installation

1. Copy the adlists.list file to /etc/pihole/adlists.list

2. Copy the 03-pihole-wildcard.conf file to /etc/dnsmasq.d/03-pihole-wildcard.conf

3. Copy the whitelist.txt file to /etc/pihole/whitelist.txt

2. Run 'sudo pihole -g' to recompile blocklist

- - - -

*(Updated 22/12/2016)*